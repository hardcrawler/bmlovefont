Simple bitmap font renderer for Love2D.  

Created primarily to render the output of the Glyph Designer program: 
https://71squared.com/en/glyphdesigner  

Should also work with the output of BmFont:  http://www.angelcode.com/products/bmfont/ 
