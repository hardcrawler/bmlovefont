local BmFont = {}


function BmFont:renderLetter( c )
	--To render a space, just increment the current textPos by the font's space width
	if c == ' ' then
		self.textPos = self.textPos + self.fontData["space"]["xadvance"]
		return
	end
	
	local f = self.fontData[c]

	local xOffset = f["xoffset"]
	local yOffset = f['yoffset']
	--Per Glyph Designer support, the Lua export is designed for Corona SDK, which uses
	--A different yOffset behavior than bmFont
	if self.isLua then
		yOffset = yOffset - (f['height']/2)
	end

	local kerningOffset = 0
	if self.lastLetter then
		local entry = self.kerningLookup[ tostring(self.fontData[self.lastLetter].id) .. "_" .. tostring(self.fontData[c].id)]
		if entry then
			kerningOffset = entry.amount
		end
	end

	local quad = love.graphics.newQuad(f['x'], f['y'], f['width'], f['height'], self.fontImage:getWidth(), self.fontImage:getHeight())
	love.graphics.draw(self.fontImage, quad, self.x + self.textPos + xOffset + kerningOffset, self.y + yOffset)
	
	local xadvance  = self.fontData[ c ]["xadvance"]
	--Store last letter rendered for kerning info
	self.lastLetter = c

	self.textPos = self.textPos + xadvance
end --drawLetter

--Just a helper method so users don't have to know innards of data structure
function BmFont:getLineHeight()
	local lineHeight = self.fontClass.common.lineHeight	
	return lineHeight
end

function BmFont:drawText( x, y, text )
	local i

	self.x = x
	self.y = y
	self.textPos = 0 --Reset internal x position variable
	local lineHeight = self.fontClass.common.lineHeight	
	local base = self.fontClass.common.base
	local width = 700

	--Debug drawing of line start, font base (line where letters are drawn)
	if self.debug == true then
		love.graphics.setColor(255,0,0,255);
		love.graphics.line( x, y+base, x+width, y+base)
		love.graphics.setColor(255,255,255,255);
		love.graphics.line( x, y, x+width, y)
		love.graphics.line( x, y+lineHeight, x+width, y+lineHeight)
	end

	--Reset kerning last letter before beginning to render
	self.lastLetter = nil

	for i = 1, #text do
		local c = text:sub(i,i)
		self:renderLetter( c )
	end
end

function BmFont:_loadFromLua(path)
	--By convention, path should be the relative path of a lua file minus the .lua extension
	self.fontClass = require(path)
	self.fontData = {}
	self.isLua = true

	for i = 1, # self.fontClass.chars do
		local theRecord = self.fontClass.chars[i]
		local theChar = theRecord["letter"]
		self.fontData[ theChar ] = theRecord
	end
	self.kerningLookup = {}
	if self.fontClass.kerning then
		for i = 1, # self.fontClass.kerning do
			local entry = self.fontClass.kerning[i]	
			--Use foo_bar as the hash in to build a quick kerning lookup
			self.kerningLookup[ tostring(entry.first) .. '_' .. tostring(entry.second)  ] = entry
		end
	end
end

--Helper function to get an element because XML is shit, and fuck XML
function BmFont:_getXmlElement(node, name)
	local element

	for i = 1, #node.kids do
		local child = node.kids[i]
		if child.name == name then
			return child
		end
	end

	assert(false, "Unable to find element " .. tostring(name))
end

function BmFont:_loadXmlFromFile( file )
	local SLAXML = require('libs/slaxdom')

	local xml = file:read('*all')
	local doc = SLAXML:dom(xml)
	assert(doc, "Error parsing XML")
	local root = doc.root
	
	local chars = self:_getXmlElement(root, "chars")

	self.fontData = {}
	
	for i = 1, #chars.kids do
		if chars.kids[i].name == "char" then
			local char = chars.kids[i]
			local record = {}
			for k, v in pairs(char.attr) do
				if type(k) == "string" then
					record[k] = v
				end
			end
			self.fontData[ record.letter ] = record
		end
	end
end

function BmFont:loadFont(path)
	local bmFont = {}

	bmFont.fontImage = love.graphics.newImage( path .. ".png")

	--Quasi "inheritance" of BmFont "class" so bmFont can call methods on itself
	setmetatable(bmFont, {__index = function (table, key) return BmFont[key] end } )

	local file = io.open(path .. ".xml")
	--Load from XML if present (currently doing this because lua output of GlyphDesigner is bugged)
	if file then
		bmFont:_loadXmlFromFile( file )
	else
		bmFont:_loadFromLua(path)
	end

	return bmFont
end

return BmFont
